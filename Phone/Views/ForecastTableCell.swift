//
//  ForecastTableCell.swift
//  weather
//
//  Created by Darren Tuck on 2021/02/16.
//  Copyright © 2021 BitLab. All rights reserved.
//

import Foundation
import UIKit

class ForecastTableCell: UITableViewCell {
    
    public static let identifier = "ForecastTableCell"
    
    @IBOutlet var dayLabel: UILabel?
    @IBOutlet var temperatureLabel: UILabel?
    @IBOutlet var weatherImageView: UIImageView?
    
    override func awakeFromNib() {
        self.backgroundColor = .clear
    }
    
    func setup(data: ForecastWeatherResponse.DailyData) {
        let time = data.dt
        let date = Date(timeIntervalSince1970: TimeInterval(time))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d"
             
        self.dayLabel?.text = dateFormatter.string(from: date)
        self.dayLabel?.textColor = .white
        self.temperatureLabel?.text = "\(Int(data.temp.max)) / \(Int(data.temp.min)) °"
        self.temperatureLabel?.textColor = .white
        self.weatherImageView?.image = UIImage(named: rowImageFromConditionId(id: data.weather[0].id))
    }

    func rowImageFromConditionId(id: Int) -> String {
        //OpenWeather id list.  2xx=storms, 3xx=drizzle, 4xx=rain
        if id >= 200 && id <= 599 {
            return "rain"
        } //6xx=snow, 7xx=atmosphere
        else if id <= 799 {
            return "rain"
        }
        // 800=clear
        else if id == 800 {
            return "clear"
        }
        // 8xx=clouds
        else if id >= 801 {
            return "partlysunny"
        }
        return "clear"
        
    }
    
}
