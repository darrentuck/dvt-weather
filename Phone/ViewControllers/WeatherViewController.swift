//
//  WeatherViewController.swift
//  weather
//
//  Created by Darren Tuck on 2021/02/15.
//  Copyright © 2021 BitLab. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class WeatherViewController: UIViewController, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var heroImageView: UIImageView!
    @IBOutlet var forecastTable: UITableView!
    @IBOutlet var locationUnavailableView: UIView!

    lazy var locationManager = CLLocationManager()
    
    private var currentWeatherData: CurrentWeatherResponse? = nil
    private var forecastWeatherData: ForecastWeatherResponse? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationUnavailableView.isHidden = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        let status = manager.authorizationStatus
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationUnavailableView.isHidden = true
            forecastTable.isHidden = false
            reloadData()
        }
        else {
            forecastTable.isHidden = true
            locationUnavailableView.isHidden = false
        }
        
    }
    
    func reloadData() {
        forecastTable.isHidden = true

        temperatureLabel.text = "--"
        descriptionLabel.text = "---"

        Api.get(path: "weather") { (data, urlResponse, error) in
            DispatchQueue.main.async {
                self.currentWeatherData = try? JSONDecoder().decode(CurrentWeatherResponse.self, from: data!)
                self.temperatureLabel.text = "\(Int(self.currentWeatherData!.main.feelsLike))°"
                self.descriptionLabel.text = self.currentWeatherData?.weather[0].weatherDescription.uppercased() ?? "---"
                
                self.heroImageView.image = UIImage(named: self.backgroundImageFromConditionId(id: self.currentWeatherData?.weather[0].id ?? 0))
            }
        }

        Api.get(path: "forecast/daily") { (data, urlResponse, error) in
            DispatchQueue.main.async {
                self.forecastTable.isHidden = false
                self.forecastWeatherData = try? JSONDecoder().decode(ForecastWeatherResponse.self, from: data!)
                self.forecastTable.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (forecastWeatherData?.list.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dayWeather = forecastWeatherData?.list[indexPath.row]
        
        let cell = forecastTable.dequeueReusableCell(withIdentifier: ForecastTableCell.identifier, for: indexPath) as! ForecastTableCell

        cell.setup(data: dayWeather!)
        return cell
    }
    
    @IBAction func locationTapped(_ sender: Any) {
        if let bundleId = Bundle.main.bundleIdentifier,
           let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(bundleId)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func backgroundImageFromConditionId(id: Int) -> String {
        //OpenWeather id list.  2xx=storms, 3xx=drizzle, 4xx=rain
        if id >= 200 && id <= 599 {
            return "sea_rainy"
        } //6xx=snow, 7xx=atmosphere
        else if id <= 799 {
            return "sea_cloudy"
        }
        // 800=clear
        else if id == 800 {
            return "sea_sunny"
        }
        // 8xx=clouds
        else if id >= 801 {
            return "sea_cloudy"
        }
        return "sea_sunny"
        
    }
}
