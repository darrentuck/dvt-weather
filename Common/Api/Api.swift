//
//  Api.swift
//  weather
//
//  Created by Darren Tuck on 2020/07/11.
//  Copyright © 2020 BitLab. All rights reserved.
//

import Foundation

class Api {
    
    public static func get(path: String, paramString: String?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {

        var urlString = "\(Constants.apiUrlv1)\(path)"
        urlString += "?appid=\(Constants.apiKey)"
        if paramString != nil {
            urlString += "&\(paramString ?? "")"
        }
        
        urlString += "&lat=-33.918861"
        urlString += "&lon=18.423300"
        urlString += "&units=metric"

        
        let fullUrl = URL(string: urlString)!
        let session = URLSession.shared
        let request = URLRequest(url: fullUrl)

        let task = session.dataTask(with: request, completionHandler: completionHandler)
        task.resume()
    }
    
    public static func get(path: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        get(path: path, paramString: nil, completionHandler: completionHandler)
    }
    
}
