//
//  CurrentWeatherResponse.swift
//  weather
//
//  Created by Darren Tuck on 2021/02/15.
//  Copyright © 2021 BitLab. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let currentWeatherResponse = try? newJSONDecoder().decode(CurrentWeatherResponse.self, from: jsonData)

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let forecastWeatherResponse = try? newJSONDecoder().decode(ForecastWeatherResponse.self, from: jsonData)

import Foundation

// MARK: - ForecastWeatherResponse
struct ForecastWeatherResponse: Codable {
    let city: City
    let cod: String
    let message: Double
    let cnt: Int
    let list: [DailyData]

    // MARK: - City
    struct City: Codable {
        let id: Int
        let name: String
        let coord: Coord
        let country: String
        let population, timezone: Int
    }

    // MARK: - Coord
    struct Coord: Codable {
        let lon, lat: Double
    }

    // MARK: - List
    struct DailyData: Codable {
        let dt, sunrise, sunset: Int
        let temp: Temp
        let feelsLike: FeelsLike
        let pressure, humidity: Int
        let weather: [Weather]
        let speed: Double
        let deg, clouds: Int
        let pop: Double
        let rain: Double?

        enum CodingKeys: String, CodingKey {
            case dt, sunrise, sunset, temp
            case feelsLike = "feels_like"
            case pressure, humidity, weather, speed, deg, clouds, pop, rain
        }
    }

    // MARK: - FeelsLike
    struct FeelsLike: Codable {
        let day, night, eve, morn: Double
    }

    // MARK: - Temp
    struct Temp: Codable {
        let day, min, max, night: Double
        let eve, morn: Double
    }

    // MARK: - Weather
    struct Weather: Codable {
        let id: Int
        let main, weatherDescription, icon: String

        enum CodingKeys: String, CodingKey {
            case id, main
            case weatherDescription = "description"
            case icon
        }
    }
}
